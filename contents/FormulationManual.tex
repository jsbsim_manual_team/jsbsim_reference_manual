%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ../JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
\chapter{Formulation manual}
\label{ch:Formulation:Manual}
\ChapFrame{\textcolor{black}{\bfseries\scshape Formulation Manual}}% NOTE: needs _three_ LaTeX passes

\section{Overview}

The core purpose of a flight dynamics model is to propagate and track the path of a flying craft over
the surface of the Earth (or another planet), given the forces and moments that act on the vehicle.
We know the characteristics of the aircraft, and we know the characteristics of the planet (gravity, rotation
rate, etc.).
And it is expected that the reader is familiar with rigid body dynamics, where moving reference frames are
involved. Still, putting all of the pieces together in a flight simulator can be overwhelming and tedious.

This section discusses aerospace vehicle equations of motion, as implemented in the class
\verb!JSBSim::FGPropagate!, using the quaternion, matrix, vector, and location math classes provided in JSBSim.
Many of the equations listed in the following sections related to the rigid body equations of motion are
referenced to Brian Stevens' and Frank Lewis' textbook, 
``Aircraft Control and Simulation'', Second Edition (2003) \cite{Book:Stevens:Lewis:2003},
and similarly to many other books such as \cite{Book:Roskam:Airplane:Flight:Dynamics:2003}
and \cite{Book:Tewari:2007}.

The notation used in this reference manual is the same that Stevens and Lewis use:
\begin{compactitem}% needs package paralist
\item
The lower right subscript, e.g. like in $\boldsymbol{v}_{\CM/\earth}$, describes the object or frame
relationship of the parameter. In the example given the parameter $\boldsymbol{v}$ represents a velocity and
$\boldsymbol{v}_{\CM/\earth}$ is the velocity of the center of mass (CM) with respect to the standard
Earth-Centered-Earth-Fixed (ECEF) frame.

\item
The upper right superscript, e.g. like in $\boldsymbol{v}^{\Body}$,
refers to a coordinate system. That is, it states which coordinate system the motion is expressed in.
In the example $\boldsymbol{v}^{\Body}$ is a velocity to be expressed in the aircraft body-fixed frame.

\item
The left superscript specifies the frame in which a derivative is taken. For instance,
$\prescript{\Body}{}{\dot{\boldsymbol{v}}}_{\CM/\earth}^{\Body}$ is the time derivative taken by
a body-fixed observer of $\boldsymbol{v}_{\CM/\earth}$ and the resulting vector is expressed in
the body-frame.
\end{compactitem}

\smallskip

There are several $\omega$ contributions to consider in the equations that follow.
These are distinguished by giving to the angular velocity vector different subscripts.
The subscripts refer to four frames of interest:
\noindent
\begin{asparablank}% needs package paralist
\setlength{\itemindent}{0pt}% this setting is local to the current environment
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{$(\;)^\Vertical$}}\hspace{0.7em}]
The vehicle NED (North, East, Down) frame --- also known as the Local Vertical frame, with the origin at
the vehicle mass center, the $x$-axis pointing North, the $y$-axis pointing east, and the $z$-axis positive
downward by the right hand rule. This is very similar to the ``local'' and ``nav'' frames in JSBSim.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{$(\;)^\Body$}}\hspace{0.7em}]
The body-fixed frame, with the $x$-axis positive forwards out the nose of the aircraft, 
the $y$-axis positive out the right side of the aircraft, and the $z$-axis positive from head to feet
of the pilot.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{$(\;)^\earth$}}\hspace{0.7em}]
The Earth Centered, Earth Fixed (ECEF) frame, with the $z$-axis coincident with the
Earth spin axis and positive North, the $x$-axis positive through the point on Earth surface
at longitude $\mu=0$ and latitude $\lambda=0$. 
This frame rotates with the Earth at a constant rate and does not translate.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{$(\;)^\Inertial$}}\hspace{0.7em}]
The Earth Centered Inertial (ECI) frame is fixed in celestial space with the $z$-axis positive North
and coincident with the spin axis, with the $x$- and $y$- axes located in the equatorial plane.
At time $t=0$ the ECI and ECEF frames are coincident.
\end{asparablank}

Some of the above frames of reference are represented in Figure~\vref{fig:Frames:Definitions}.
%---------------------------------------------------------------------------------
\begin{figure}[t]
   \centering
   \includegraphics[width=0.6\textwidth]{images/earth_frames.pdf}
   \caption{The Eart-Centered, Earth-Fixed frame and the local Vertical (NED) frame.}\label{fig:Frames:Definitions}
\end{figure}
%---------------------------------------------------------------------------------

Examining the $\omega$ contributions:
\begin{asparablank}% needs package paralist
\setlength{\itemindent}{0pt}% this setting is local to the current environment
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{%
    % $\boldsymbol{\omega}_{\Body/\Vertical}$%
    \gls{omega:Body:To:Vertical}% get the symbol from glossary
}}\hspace{1.0em}]
The angular velocity vector of the body-fixed frame relative to the vehicle NED (local) frame.
Note that when the vehicle is not manuevering (such as at-rest on a runway or launch pad), this angular
velocity vector is zero.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{%
    % $\boldsymbol{\omega}_{\Body/\earth}$%
    \gls{omega:Body:To:Earth}% get the symbol from glossary
}}\hspace{0.7em}]
The angular velocity vector of the body-fixed frame relative to the ECEF frame. 
Note that when the vehicle is at rest on the surface this angular velocity vector is zero.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{%
    %$\boldsymbol{\omega}_{\Body/\Inertial}$%
    \gls{omega:Body:To:Inertial}% get the symbol from glossary
}}\hspace{0.7em}]
The angular velocity vector of the body-fixed frame relative to the ECI frame.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{%
    % $\boldsymbol{\omega}_{\Vertical/\earth}$%
    \gls{omega:Vertical:To:Earth}% get the symbol from glossary
}}\hspace{0.7em}]
The angular velocity vector of the vehicle NED (local) frame relative to the ECEF frame.
Note that this angular velocity vector is independent of the vehicle rotational motion, and it is a function
of the vehicle velocity relative to the ECEF frame --– the speed at which the frame travels across the curved
surface of the Earth --– which of course implies it has an angular velocity of its own.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[2.5em][c]{
    %$\boldsymbol{\omega}_{\earth/\Inertial}$%
    \gls{omega:Earth:To:Inertial}% get the symbol from glossary
}}\hspace{0.7em}]
The angular velocity vector of the ECEF frame relative to the ECI frame. 
This is simply the angular velocity of the Earth about its spin axis.
\end{asparablank}

\smallskip

We will see the use of the cross-product matrix in the equations that follow. The cross-product matrix
$\widetilde{\boldsymbol{\Omega}}$ is the skew-symmetric matrix:
\begin{equation}\label{eq:EOM:Omega:Cross:Product}
\widetilde{\boldsymbol{\Omega}} =
\left[
    \begin{array}{ccc}
       \phantom{-}0 &           -R & \phantom{-}Q \\
       \phantom{-}R & \phantom{-}0 &           -P \\
                 -Q & \phantom{-}P & \phantom{-}0
    \end{array}
\right]
\end{equation}
where $P$, $Q$, and $R$ are the body rates relative to the inertial frame, expressed in the body frame.
i.e.~the components of vector $\boldsymbol{\omega}_{\Body/\Inertial}^{\Body}$.

Now, some discussion is needed of the variables used in JSBSim:
\begin{asparablank}% needs package paralist
\setlength{\itemindent}{0pt}% this setting is local to the current environment
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{vUVW}%
    \gls{var:vUVW}% get the symbol from glossary
}}\hspace{1.0em}]
The body-fixed frame velocity vector of the vehicle relative to the ECEF frame, expressed in the body-fixed
frame. With zero winds, and while the aircraft is at rest on the runway, this 3 element column vector will
show zero velocity.
A vehicle in a stable, equatorial, low Earth orbit will show a total magnitude for vUVW that is less than
that required to maintain the orbit, but the total magnitude of the velocity relative to the inertial frame
will show the true, inertial orbital velocity. In Stevens and Lewis notation, this is:
\gls{V:Body:To:Earth:Body}.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{vPQR}%
    \gls{var:vPQR}% get the symbol from glossary
}}\hspace{1.0em}]
The body-fixed frame angular velocity vector of the vehicle relative to the ECEF frame, expressed in the body frame. While the aircraft is at rest on the runway, this 3 element column vector will show zero angular velocity. In Stevens and Lewis notation, this is: \gls{omega:Body:To:Earth:Body}.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{vPQRi}%
    \gls{var:vPQRi}% get the symbol from glossary
}}\hspace{1.0em}]
The body-fixed frame angular velocity vector of the vehicle relative to the ECI frame, expressed in the body frame. While the aircraft is at rest on the runway, this 3 element column vector will show a non-zero angular velocity. In Stevens and Lewis notation, this is: \gls{omega:Body:To:Inertial:Body}.
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{vVel}%
    \gls{var:vVel}% get the symbol from glossary
}}\hspace{1.0em}]
The body-fixed frame velocity vector of the vehicle relative to the ECEF frame, expressed in the
vehicle-carried NED (local) frame. In Stevens and Lewis notation, this is: \gls{V:Body:To:Earth:Vertical}.
\end{asparablank}

\smallskip

JSBSim provides several rotation matrices for use in moving from one reference frame to another. 
For instance, the \verb!Tec2b! matrix (represented in JSBSim through the use of an \verb!FGMatrix33! object)
is the transformation matrix from ECEF to body. This matrix is represented in Stevens and Lewis as 
\gls{C:Body:To:Earth} --– or the transformation matrix relating the body frame orientation with respect to the ECEF frame.

The transformation matrices provided in JSBSim are ``owned'' by two specific objects: the location object (an instance of an \verb!FGLocation! class) and the attitude object (an instance of an \verb|FGQuaternion| object).
The location object stores the location of a vehicle in the ECEF frame. Therefore, the location object is aware
of the relative orientation of the geographic reference frames relative to each other.
The geographic frames are the inertial frame (ECI), the Earth-fixed frame (ECEF), and the local frame (NED).
The attitude quaternion object stores the attitude of the vehicle body frame relative to the inertial frame.
Using the transformation matrices from both frames, it is possible to determine the orientation of any of the
frames relative to each other from the stored transformation matrices.
The matrices stored in the location object are:

\begin{asparablank}% needs package paralist
\setlength{\itemindent}{0pt}% this setting is local to the current environment
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{Ti2ec}%
    \gls{var:Ti2ec}% get the symbol from glossary
}}\hspace{1.0em}]
The ECI (inertial) to ECEF frame transformation matrix (defines the orientation of the ECEF frame
with respect to the ECI frame).
%
\item[\fcolorbox{gray!30}{gray!30}{\makebox[3em][c]{%
    % \texttt{Tec2i}%
    \gls{var:Tec2i}% get the symbol from glossary
}}\hspace{1.0em}]
The transpose of the \gls{var:Ti2ec} matrix, i.e.~the ECEF to ECI (inertial) frame transformation matrix.
\end{asparablank}

\medskip 

\TODOInline[ADM]{Finish this part. Note how glossary entry are treated.}

\medskip

\TODOInline[ADM]{This part should be extended with a minimal presentation of the Theory behind.}

\section{Equations of motion}

The core purpose of a flight dynamics model is to propagate and track the path of a flying craft over the surface of the Earth (or another planet), given the forces and moments that act on the vehicle. We know the characteristics of the aircraft, and we know the characteristics of the planet (gravity, rotation rate, etc.). And it is expected that the reader is familiar with rigid body dynamics, where moving reference frames are involved. Still, putting all of the pieces together in a flight simulator can be overwhelming and tedious.

Now let me try an equation in \LaTeX:
%%
%% see http://tex.stackexchange.com/questions/11542/left-and-right-subscript
%%
%% macros will be included for easy typesetting of conventional symbols
\begin{equation}\label{eq:EOM:Translation}
\prescript{\Body}{}{\dot{\boldsymbol{v}}}_{\CM/\earth}^{\Body}
   =\frac{1}{m} \boldsymbol{F}_{\Aero,\Thrust}^{\Body}
      -\left( 
         \widetilde{ \boldsymbol{\Omega} }_{\Body/\Inertial}^{\Body}
         + 
         \widetilde{ \boldsymbol{\Omega} }_{\CM/\Inertial}^{\Body}
     \right)
        \boldsymbol{v}_{\CM/\earth}^{\Body}
        + \boldsymbol{C}_{\Body/\Inertial} \, \boldsymbol{g}^{\Inertial}
\end{equation}

With the following formula we express the rolling moment $\mathcal{L}$
in terms of a rolling moment coefficient, of flight dynamic pressure
$\bar{q}=\dfrac{1}{2}\rho V^2$, and of airframe geometric constants
$S$ and $b$:
\begin{equation}\label{eq:EOM:Rolling:Moment}
\mathcal{L} 
   = C_{\mathcal{L}} \,
      \frac{1}{2}\rho V_{\text{\scshape t}}^2 \, S \, b
\end{equation}

\begin{figure}[t]
   \centering
   \includegraphics[width=\textwidth]{images/ac_basic_definitions.pdf}
   \caption{Basic definitions.}\label{fig:AC:Basic:Definitions}
\end{figure}

\lipsum[1]

\subsection{Translational acceleration}
\lipsum[1]

\subsection{Angular acceletarion}
\lipsum[1]

\subsection{Translational velocity}
\lipsum[1]

\subsection{Angular velocity}
\lipsum[1]

\lipsum[1]

\medskip

\TODOInline[ADM]{Finish this part.}

\medskip

Now I want to test the package \textsf{glossaries}.
This is \gls{x:Body}. This is \gls{acr:AIAA}.
And we have \gls{acr:MST} and \gls{acr:GNC}.
This is a \gls{Motion:Platform}.

This is \gls{x:Stability}.

Or we have also \gls{acr:DAVE-ML}.

%\lipsum[1]

\medskip

\TODOInline[ADM]{taken from Wikipedia.\newline
\url{http://en.wikipedia.org/wiki/Flight_sim}\newline
All histories of FS coincide with the version reported in the book by Rolfe and Staples
\cite{Book:Rolfe:Staples:1988}. If we want to have this section we really need to add original stuff here.}

\medskip

According to \citet{Book:Rolfe:Staples:1988}, flight simulation never fails.
\citeauthor{Book:Rolfe:Staples:1988}, the authors of \citetitle{Book:Rolfe:Staples:1988},
claim that flight simulation never fails.
As said elsewhere \citep[see][]{Book:Rolfe:Staples:1988}
flight simulation never fails.

Now some stuff for testing the index.
To solve various problems in physics, it can be advantageous
to express any arbitrary piecewise-smooth function as a Fourier Series
\index{Fourier Series}
composed of multiples of sine and cosine functions.


%% EOF