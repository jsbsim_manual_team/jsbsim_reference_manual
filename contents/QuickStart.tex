%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ../JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
\chapter{Quickstart}
\label{ch:Quickstart}
\ChapFrame{\textcolor{black}{\bfseries\scshape Quickstart}}% NOTE: needs _three_ LaTeX passes

\section{Getting the source}

JSBSim can be downloaded as source code in a ``release'' --- a bundled set of source code files (a snapshot) that was taken at some point in time when the source code was thought to be stable. A release is made at random times as the developers see fit. The code can be downloaded as a release from the project web site at:

\noindent
\adjustbox{center}{% a way of making some short stuff centered horizontally
   \url{http://sourceforge.net/project/showfiles.php?group_id=19399}
}

The source code can also be retrieved from the revision management storage location using a software tool called \prog{cvs} (Concurrent Versions System, CVS). The program \prog{cvs} is available as an optional tool in the Cygwin environment under Windows, but is usually standard with Linux systems. Directions on downloading JSBSim can be found at the project web site at \url{http://www.jsbsim.org}. Briefly, the JSBSim code (including example files needed to run JSBSim) can be downloaded using cvs as follows:

\smallskip

%----------------------------------------------------------------------------------
% Here we use the environement myCommandLine defined by us to show examples
% of command line sequences. See file "_lst_defs.tex".
%----------------------------------------------------------------------------------
\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{cvs}}» \ »{\color{blue}\meta{enter}}»
   -d:pserver:anonymous@jsbsim.cvs.sourceforge.net:/cvsroot/jsbsim \ »{\color{blue}\meta{enter}}»
   login »{\color{blue}\meta{enter}}»
   »{\color{blue}\meta{enter} \textrm{(when prompted for a password)}}»
$ »{\color{red!60!black}\textbf{cvs}}» -z3 \ »{\color{blue}\meta{enter}}»
   -d:pserver:anonymous@jsbsim.cvs.sourceforge.net:/cvsroot/jsbsim \ »{\color{blue}\meta{enter}}»
   co -P JSBSim »{\color{blue}\meta{enter}}»
\end{myCommandLine}
%----------------------------------------------------------------------------------
% Here we clarify some typographical stuff
%----------------------------------------------------------------------------------
{\color{gray}\hrule}
\noindent
\begin{minipage}{\linewidth}\footnotesize%\scriptsize
%\setstretch{0.9}
\rule{0pt}{0.9em}
\ding{42}\ In the example above each single command is typeset on multiple lines for clarity.
In a Bash shell users usually type their commands on a unique line with no need to give a carriage return. Or,
as done in the example, they can give \texttt{\textvisiblespace\textbackslash\meta{enter}} in the middle
of a command and continue on a new line.
\rule[-3.5pt]{0pt}{0.9em}
\end{minipage}
\setstretch{\mynormalstretch}
{\color{gray}\hrule}

\medskip

Alternatively –-- and perhaps more easily –-- a tarball can be downloaded that contains the very latest version of the code as it exists in the CVS repository at: \url{http://jsbsim.cvs.sourceforge.net/jsbsim/}. Click on the Download GNU tarball text. The tarball can be extracted using the \prog{tar} program, or another extractor such as \prog{7zip}.

\section{Getting the program}
At the same location, executable files for some platforms (Windows, Cygwin, Linux, IRIX, etc.) may also be downloaded. It should be noted that at present (and unless otherwise stated in the release notes for a release) the example aircraft and script files, etc. must be downloaded as part of the source code release. Another alternative is to check out the files from the JSBSim CVS repository.
\section{Building the program}
JSBSim can be built using standard GNU tools, \prog{automake}, \prog{autoconf}, \prog{g++}, \prog{make}, etc. There also exists a project file for building JSBSim using the Visual C++ development environment from Microsoft. For a Unix environment (Cygwin, Linux, etc.), once you have the source code files, you should be able to change directory
(``cd'') to the JSBSim base directory and type:

\smallskip

\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{autogen.sh}}» »{\color{blue}\meta{enter}}»
   »{\color{blue}\textrm{(wait for a sequence of configuration messages)}}»
$ »{\color{red!60!black}\textbf{make}}» »{\color{blue}\meta{enter}}»
\end{myCommandLine}

\smallskip

Note that the \verb!make! command can be told to use all available cores if your CPU is multi-core. Specify the 
%% “-j \#” 
``\verb!-j #!''
option, where the ``\verb!#!'' is replaced with the number of cores you have plus one. 
For a quad core of course this would lead to the make command:

\smallskip

\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{make}}» –j 5 »{\color{blue}\meta{enter}}»
\end{myCommandLine}

\smallskip

The base directory is the location under which all other files and directories are found. When JSBSim source code is downloaded or checked out from CVS, the exact name of the directory may vary, and there may be intermediate directories under which the main JSBSim directory is placed. To be clear, the JSBSim root directory is the one where the \verb!autogen.sh! files is to be found, and under which the \verb!src/! subdirectory will be found.
We may call it \meta{JSBSim root} generically and depends on how the user has retrieved and unpacked the JSBSim
distribution.
When the \verb!autogen! and \verb!make! commands are run as directed, this should result in an executable being created under the \meta{JSBSim root}\verb!/src/! subdirectory.
If you are building JSBSim under \prog{Microsoft Visual C++}, the executable will be placed under the 
\meta{JSBSim root}\verb!/Debug/! or \meta{JSBSim root}\verb!/Release/! subdirectory, depending on how you are building
the executable. Project files for \prog{Microsoft Visual C++} are found in the JSBSim root directory, and are named \verb!JSBSim.vcproj! (for \prog{Microsoft Visual C++ 2008 Express}), and \verb!JBSim.vcxproj! (for \prog{Microsoft Visual C++ 2010 Express}).

\smallskip

Note: If you would like to optimize your executable for speed, and you know the architecture for your computer (for instance, “nocona”), you could do that as follows:

\smallskip

\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{autogen.sh}}» CFLAGS="-O3 -march=nocona" \  »{\color{blue}\meta{enter}}»
               CPPFLAGS="-O3 -march=nocona" \ »{\color{blue}\meta{enter}}»
               CXXFLAGS="-O3 -march=nocona" »{\color{blue}\meta{enter}}»
\end{myCommandLine}

\smallskip
\noindent
The ``\verb!nocona!'' option supports compilation for 64-bit extensions, and for the MMX, SSE, SSE2, and SSE3 instruction sets. You can find out more about the GNU compilers and their configuration for specific architerctures at \url{http://gcc.gnu.org}.

\section{Running the program}

There may be several options specified when running the standalone \prog{JSBSim} application that is provided with a JSBSim release or built using the instructions above. 
If you have built \prog{JSBSim} from source code there will be an executable under the \verb!src/! subdirectory. 
If the program \prog{JSBSim} is copied in JSBSim root directory and is launched with the following command line:

\smallskip

\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{JSBSim}}» --help »{\color{blue}\meta{enter}}»
\end{myCommandLine}

\smallskip
\noindent
one gets the following output:

{\footnotesize
\begin{Verbatim}
Usage (items in brackets are optional):

  JSBSim [script name] [output directive file names] <options>

 Options:
 --help Returns a usage message
 --version Returns the version number
 --outputlogfile=<filename> Sets/replaces the name of the first (or only) data log file
 --root=<path> Sets the JSBSim root directory (where src/ resides)
 --aircraft=<filename> Sets the name of the aircraft to be modeled
 --realtime Specifies to run in actual real world time
 --nice Directs JSBSim to run at low CPU usage
 --suspend Specifies to suspend the simulation after initialization
 --initfile=<filename> Specifies an initialization file to use
 --catalog Directs JSBSim to list all properties for this model
     (--catalog can be specified on the command line along with a
      --aircraft option, or by itself, while also specifying the
      aircraft name, e.g. --catalog=c172)
 --end-time=<time> Specifies the sim end time (e.g. time=20.5)
 --property=<name=value> Sets a property to a value.
     For example: --property=simulation/integrator/rate/rotational=1
 --simulation-rate=<rate (double)> specifies the sim dT time or frequency
      If rate specified is less than 1, it is interpreted as
      a time step size, otherwise it is assumed to be a rate in Hertz.

NOTE: There can be no spaces around the = sign when an option is followed by a filename
\end{Verbatim}
}% end-of-footnotesize

If you have built JSBSim from source code there will be an executable under the src/ subdirectory. You can run \prog{JSBSim}
by supplying the name of a script: 
\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{JSBSim}}» scripts/c1723.xml »{\color{blue}\meta{enter}}»
\end{myCommandLine}

If you are running the program using the Microsoft Visual C++ IDE, you will need to go to  Program Options and set the Command
Arguments to run the script as shown above. 

You may have simply downloaded the executable. In this case, you should create the \verb!scripts/!, \verb!aircraft/!,
and \verb!engine!, subdirectories.
You should place aircraft models under the \verb!aircraft/! subdirectory, in another subdirectory that has the same
name as the aircraft. For instance, if you create a B-2 flight model, you would place that in the 
\verb!aircraft/B2/! subdirectory, and the aircraft flight model name would be called \verb!B2.xml!.
Or, as in Figure~\vref{fig:JSBSim:Source:Tree}, if you have a model of Tecnam P2006T you might want to name the main
configuration file \verb!p2006t.xml! and put it into the folder \meta{JSBSim root}\verb!/aircraft/p2006t/!.

%---------------------------------------------------------------------------------
\begin{figure}[t]
   \centering
   \includegraphics[width=0.7\textwidth]{images/jsbsim_tree.pdf}
   \caption{JSBSim source tree.}\label{fig:JSBSim:Source:Tree}
\end{figure}
%---------------------------------------------------------------------------------

\section{Getting support}

The best way to get support on JSBSim is to subscribe and post questions to the relevant mailing list.
You can find out about the mailing lists dedicated to JSBSim at the following Sourceforge address: \url{http://www.sf.net/mail/?group_id=19399}.
%% EOF
