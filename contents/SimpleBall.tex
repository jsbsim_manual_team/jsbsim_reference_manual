%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ../JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
%
\section{Simple ball}
%\label{ch:Simple ball}
%\ChapFrame{\textcolor{black}{\bfseries\scshape Simple ball}}% NOTE: needs _three_ LaTeX passes

Let us now consider the example of a simple body thrown into the air with a given velocity.
We call this example the ``simple ball example.'' This ball will have no control or propulsion systems, will exhibit only aerodynamic drag, and will have a single contact point. 

The initial conditions of the simple ball example are depicted in Figure~\ref{fig:Examples:Simple:Ball}.
%======================================
\begin{figure}[t]
   \centering
   \includegraphics[width=0.5\textwidth]{images/simple_ball.pdf}
   \caption{Initial conditions of the simple ball example.}\label{fig:Examples:Simple:Ball}
\end{figure}
%=======================================
\subsection{Setting the inputs}
The files needed by this case study have to be prepared and located in the \verb!aircraft! and \verb!scripts! 
sub-directories of \meta{JSBSim root}. This case will be named \verb!ball_simple!, so a folder \verb!ball_simple!
has to be created under \verb!aircraft!.

To define the geometry, mass, ground reactions, aerodynamics characteristics, and the desired outputs
the file \verb!aircraft/ball_simple/ball_simple.xml! has to be edited as follows:

\smallskip

%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%  %  |\textcolor{blue}{$\leftarrow$ \textrm{informazioni sulla geometria}}|
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]

<?xml version="1.0"?>
<?xml-stylesheet 
   type="text/xsl"
   href="http://jsbsim.sourceforge.net/JSBSim.xsl"?>
<fdm_config 
  name="SIMPLE BALL"
  version="2.0"
  release="BETA"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="http://jsbsim.sourceforge.net/JSBSim.xsd">

    <fileheader>
      <author> JSBSim team </author>
      <filecreationdate> 2013-02-21 </filecreationdate>
      <version> Version 1.0 </version>
      <description> Test file for the simple ball example </description>
    </fileheader>

    <metrics>
        <wingarea unit="FT2"> 1 </wingarea>
        <wingspan unit="FT"> 1 </wingspan>
        <chord unit="FT"> 1 </chord>
        <htailarea unit="FT2"> 0 </htailarea>
        <htailarm unit="FT"> 0 </htailarm>
        <vtailarea unit="FT2"> 0 </vtailarea>
        <vtailarm unit="FT"> 0 </vtailarm>
        <location name="AERORP" unit="IN">
            <x> 0 </x>
            <y> 0 </y>
            <z> 0 </z>
        </location>
    </metrics>

    <mass_balance>
        <ixx unit="SLUG*FT2"> 10 </ixx>
        <iyy unit="SLUG*FT2"> 10 </iyy>
        <izz unit="SLUG*FT2"> 10 </izz>
        <ixy unit="SLUG*FT2"> -0 </ixy>
        <ixz unit="SLUG*FT2"> -0 </ixz>
        <iyz unit="SLUG*FT2"> -0 </iyz>
        <emptywt unit="LBS"> 50 </emptywt>
        <location name="CG" unit="IN">
            <x> 0 </x>
            <y> 0 </y>
            <z> 0 </z>
        </location>
    </mass_balance>

  <ground_reactions>
        <contact type="BOGEY" name="NOSE_CONTACT">
            <location unit="IN">
                <x> 0 </x>
                <y> 0 </y>
                <z> 0 </z>
            </location>
            <static_friction> 0 </static_friction>
            <dynamic_friction> 0 </dynamic_friction>
            <rolling_friction> 0 </rolling_friction>
            <spring_coeff unit="LBS/FT">1000 </spring_coeff> 
            <damping_coeff unit="LBS/FT/SEC"> 200000 </damping_coeff>
            <max_steer unit="DEG"> 0.0 </max_steer>
            <brake_group> NONE </brake_group>
            <retractable>0</retractable>
        </contact>
    </ground_reactions>
	
<propulsion/>

    <flight_control name="FGFCS"/>

    <aerodynamics>
        <axis name="DRAG">
            <function name="aero/coefficient/CD">
                <description>Drag</description>
                <product>
                    <property>aero/qbar-psf</property>
                    <property>metrics/Sw-sqft</property>
                    <value>0.0001</value>
                </product>
            </function>
        </axis>
    </aerodynamics>

    <output name="ball_simple_output.csv" type="CSV" rate="1">
      <property> position/h-sl-meters </property>       <!--  2 -->
      <property> velocities/u-fps </property>           <!--  3 -->
      <property> accelerations/udot-ft_sec2 </property> <!--  4 -->
      <property> forces/fbx-total-lbs </property>       <!--  5 -->
    </output>

</fdm_config>
\end{lstlisting}

This file represents a ball with a reference area (\verb!wingarea!) of \SI{1}{ft^2}, 
a lateral reference length (\verb!wingspan!) of \SI{1}{ft}, 
and a longitudinal reference length (\verb!chord!) of \SI{1}{ft}.

To change the output the user need to know the complete list of variables of the simulation.
To view this list the following command is available:

\smallskip

\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{JSBSim}}» --aircraft=ball --catalog  »{\color{blue}\meta{enter}}»
\end{myCommandLine}

It is a good idea to write the catalog in a text file for later use. This is done by simply redirecting
the Bash output to a file:

\smallskip

\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{JSBSim}}» --aircraft=ball --catalog > ball_catalog.txt »{\color{blue}\meta{enter}}»
\end{myCommandLine}

This last command creates a file \verb!ball_catalog.txt! in the \meta{JSBSim root} as follow:

{\footnotesize
\begin{Verbatim}

     JSBSim Flight Dynamics Model v1.0 Jan  5 2013 14:51:38
            [JSBSim-ML v2.0]

JSBSim startup beginning ...

  Property Catalog for ball\_simple

    velocities/h-dot-fps
    velocities/v-north-fps
    ...
    position/long-gc-rad
    position/lat-gc-deg
    ...
    metrics/terrain-radius
    metrics/Sw-sqft
    ...
    attitude/phi-rad
    attitude/theta-rad
    ...
    simulation/integrator/rate/rotational
    simulation/integrator/rate/translational
    ...
    atmosphere/T-R
    atmosphere/rho-slugs\_ft3
    ...
    propulsion/tat-r
    propulsion/tat-c
    ...
    accelerations/a-pilot-x-ft\_sec2
    accelerations/a-pilot-y-ft\_sec2
    ...
    forces/load-factor
    forces/fbx-aero-lbs
    ...
    aero/alpha-rad
    aero/beta-rad
    ...
    flight-path/gamma-rad
    flight-path/psi-gt-rad
    ...
    fcs/steer-cmd-norm
    fcs/wing-fold-pos-norm
    ...
    gear/gear-pos-norm
    gear/gear-cmd-norm
    ...
    moments/l-aero-lbsft
    moments/m-aero-lbsft
    ...
    ic/q-rad\_sec
    ic/r-rad\_sec
    ...
    trim/solver/showConvergeStatus
    trim/solver/variablePropPitch
    ...
\end{Verbatim}
}% end-of-footnotesize

We can use this file to choose the variables that we want to study, copying and pasting them into the 
\verb!<output>! section of the file \verb!aircraft/ball/ball.xml!. For instance, to have the time history
of altitude in meters we do as follows:

\smallskip
%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
< output name="ball_simple_output.csv" type="CSV" rate="1">
   <property> position/h-sl-meters </property>    
</output>
\end{lstlisting}

\smallskip
The ball description is not the only file that we need, however. We also have to tell the simulation where to begin what the initial conditions of the simulation run will be. 
We can define initial values for various parameters in the initialization file \verb!aircraft/ball/reset01.xml!. An example of such a file is as follows:

\smallskip
%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%  %  |\textcolor{blue}{$\leftarrow$ \textrm{informazioni sulla geometria}}|
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
<initialize name="reset01">
  <!--
    This file sets up the cannonball to fire.
  -->
  <ubody unit="FT/SEC">  500.0  </ubody>
  <latitude unit="DEG">    0.0  </latitude>
  <longitude unit="DEG"> -90.0  </longitude>
  <theta unit="DEG">      60.0  </theta>
  <psi unit="DEG">        40.0  </psi>
  <altitude unit="FT">    1.0  </altitude>

</initialize>
\end{lstlisting}

The file above sets the ball up with an initial velocity of  \SI{500}{ft/sec} along its own x
axis, an angle of \SI{60}{\degree} above the horizon, at a heading of \SI{40}{\degree} east of north and an altitude of \SI{1.0}{ft}.
The initial location is on the equator at \SI{90}{\degree} west longitude.

\subsection{Running the simulation }
A simulation run can be made using this command line (assuming that the jsbsim executable
is in the \meta{JSBSim root}) of a Windows shell or for example, using the \verb!Cygwin! bash. 

The file usefull to run the simulations and specially, to define the simualtion events, is \verb! scripts/cannonball.xml!. 

It's made as follow:

\smallskip

%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%  %  |\textcolor{blue}{$\leftarrow$ \textrm{informazioni sulla geometria}}|
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="http://jsbsim.sf.net/JSBSimScript.xsl"?>
<runscript xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="http://jsbsim.sf.net/JSBSimScript.xsd"
    name="cannon ball test">
  <use aircraft="ball" initialize="reset01"/>
  <run start="0.0" end="27" dt="0.00833333"/>
</runscript>

\end{lstlisting}

Make sure that the command \meta{use} is referred to \verb! aircraft/ball/ball.xml! and 
\verb! aircraft/ball/reset001.xml! edited befoure.

Finally the command to type is

\smallskip



\begin{myCommandLine}
$ ./»{\color{red!60!black}\textbf{JSBSim}}» --script=scripts/ball.xml »{\color{blue}\meta{enter}}»
\end{myCommandLine}

This command creats a \verb!.csv! output file in the \meta {JSBSim root}; in our case it will be  \verb!(ball_output.csv)!.

\subsection{Post processing}

As shown in Chapter 1, we can use prep\_plot for post-processing the data files that
JSBSim produce in our simple ball case. If we want to use the \verb!Cygwin! bash combined with gnuplot as plotting program, we have to verify that \verb!gnuplot! package is installed.

To create a prep\_plot file, type this command:

\smallskip

\begin{myCommandLine}
$ ./»{{\color{red!60!black}\textbf{prep\_plot}}{ ball\_output.csv}}» --pdf --comp | gnuplot »{\color{blue}\meta{enter}}
\end{myCommandLine}

This command creats a pdf file in the \meta {JSBSim root}; in our case it will be (\verb!ball_output.pdf!)

If we want to manage this .pdf file (for example if we want to change the range of an axis) we have to edit the instructions that \verb!jsbsim! automatically gives to gnuplot in the command shown before. we can do that generating these instructions using the command:

\smallskip

\begin{myCommandLine}
$ ./»{{\color{red!60!black}\textbf{prep\_plot.exe}}{ ball\_output.csv}}» --pdf --comp > ball_output.txt 
»{\color{blue}\meta{enter}}
\end{myCommandLine}


This command creats a file \verb!ball_output.txt! in the \meta{JSBSim root} as shown:

\smallskip

%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%  %  |\textcolor{blue}{$\leftarrow$ \textrm{informazioni sulla geometria}}|
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
set terminal pdf enhanced color rounded size 12,9 font "Helvetica,12"
set output 'ball_output.pdf'
set lmargin  13
set rmargin  4
set tmargin  4
set bmargin  4
set datafile separator ","
set grid xtics ytics
set xtics font "Helvetica,10"
set ytics font "Helvetica,10"
set timestamp "%d/%m/%y %H:%M" offset 0,1 font "Helvetica,10"
set title "h-sl-meters vs. Time" font "Helvetica,14"
set xlabel "Time (sec)" font "Helvetica,12"
set ylabel "h-sl-meters" font "Helvetica,12"
plot  "ball_simple_output.csv" using 1:2 with lines title "h-sl-meters"

\end{lstlisting}

For example, in the simple ball case, we have to change the range of the y-axis. We can do that inserting the commands line (\verb!set xrange!;\verb! set yrange!)  in the (\verb!bal_output.txt!).

Example
\smallskip

%-------------------------------------------------------------------------
% just in case, load settings for the xml pretty-printing
\input{_lst_xml}
%-------------------------------------------------------------------------
\begin{lstlisting}[%
    language=xml,%
    %tagstyle=\color{red},%
    lineskip=-1pt,%  %  |\textcolor{blue}{$\leftarrow$ \textrm{informazioni sulla geometria}}|
    escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
set terminal pdf enhanced color rounded size 12,9 font "Helvetica,12"
set output 'ball_output.pdf'
set lmargin  13
set rmargin  4
set tmargin  4
set bmargin  4
set datafile separator ","
set grid xtics ytics
set xtics font "Helvetica,10"
set ytics font "Helvetica,10"
set timestamp "%d/%m/%y %H:%M" offset 0,1 font "Helvetica,10"
set title "h-sl-meters vs. Time" font "Helvetica,14"
set xlabel "Time (sec)" font "Helvetica,12"
set ylabel "h-sl-meters" font "Helvetica,12"
set xrange [... : ...]
set yrange [... : ...]
plot  "ball_output.csv" using 1:2 with lines title "h-sl-meters"

\end{lstlisting}


Now we have to came back in the \verb!cygwin! bash and type the following command:

\smallskip

\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{gnuplot}} ball\_output.txt} {\color{blue}\meta{enter}}
\end{myCommandLine}

This command will compile the file \verb!ball_output.txt! modificated.

In the same way, fixed the (\verb!ball_output.txt!), for other simulations we can run directly:

\smallskip

\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{gnuplot}} ball\_output.txt} {\color{blue}\meta{enter}}
\end{myCommandLine}

\subsection{Results}

From the input and script files introduced above we obtain the charts of Figure~\ref{fig:Simple:Ball:Attitude}.
The plots show the altitude $h$, the $u$-velocity component, the $\dot{u}$, and the total $X$-force component 
versus time.

If we want to know the time history of the altitude at various values of    
 $\theta_0$ (at time $t=0$), or the time history of the altitude at
 various values of speed, 
we have to change the value of \verb!<theta>! and the value of \verb!<ubody>! respectively in the initialization file.
Finally we obtain the plots reported in Figure~\ref{fig:Simple:Ball:Various:Theta} 
and~\ref{fig:Simple:Ball:Altitude:Various:Speeds}.

It can be also intersting to study the different trajectories of the ball at 
various values of $\theta_0$ or at various values of speed,as shown in
Figure~\ref{fig:Simple:Ball:Altitudevsdistance} 


\FloatBarrier

\TODOInline[ADM]{Report plots of altitude versus $x$-distance.}
%==========================================================
\begin{figure}[t]
  \centering
  \includegraphics[page=1,width=\textwidth]{images/simple_ball_output.pdf}
  \caption{Time history of  all variables of output.}
  \label{fig:Simple:Ball:Attitude}
\end{figure}
%==========================================================

\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{images/simple_ball_various_theta.pdf}
  \caption{Time history of the altitude in the simple ball example at various $\theta$.}
  \label{fig:Simple:Ball:Various:Theta}
\end{figure}
%==========================================================

\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{images/simple_ball_various_speed.pdf}
  \caption{Time history of the altitude in the simple ball example at various speeds.}
  \label{fig:Simple:Ball:Altitude:Various:Speeds}
\end{figure}
%==========================================================

\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{images/simple_ball_Altitudevsdistance.pdf}
  \caption{Altitude vs distance.}
  \label{fig:Simple:Ball:Altitudevsdistance}
\end{figure}
%==========================================================

