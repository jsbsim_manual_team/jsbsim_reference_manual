This is the LaTeX version of the JSBSim reference manual
(see www.jsbsim.org).

The JSBSim documentation project aims at guiding the worldwide
community of users and developers in a correct and creative use of the 
flight dynamics software library. Moreover, according to the vision of
its principal authors, Jon S. Berndt and Agostino De Marco, the manual
has to be an attractive document and pretty to look at.

Thanks to the features of the TEX typesetting system, the JSBSim manual
has taken a flexible and highly engineered structure, which is suitable
to be progressively enriched in content by all the people that are willing 
to contribute.

The LaTEe sources are designed to handle automatically the tables of contents,
the list of tables, figures, and other floating objects, the management of
acronyms, glossary entries, and index entries. The source authoring and
typesetting workflow rely only on free tools and fonts. 

The illustrations are freely usable and stored in a dedicated folder
(including their sources when applicable).

Contributors are encouraged to clone the documentation repository, edit the
source files according to their own skills, add content and submit their
modification for the benefit of all the JSBSim users.