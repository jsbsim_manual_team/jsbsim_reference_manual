%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ./TypesettingGuidelines_JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
\chapter{Quick start}

\section{Getting the \hologo{LaTeX} sources of the JSBSim manual}

The JSBSim manual is available as a PDF file by visiting the following web address:\newline
\makebox[\textwidth][c]{%
   \url{https://bitbucket.org/jsbsim_manual_team/jsbsim_reference_manual/src}
}\newline
There you find the \hologo{LaTeX} source tree of the manual. In the main directory
you find the file:\newline
\adjustbox{center}{%
   \verb!JSBSim_Reference_Manual.pdf!
}

\smallskip

The JSBSim manual \hologo{LaTeX} repository is hosted by Bitbucket (\url{bitbucket.org}), a web-based hosting
service similar to GitHub that offers free accounts for both public and private projects.
Our project is public and is owned by a development team named
\verb!jsbsim_manual_team!.

\smallskip

The document is produced by compiling the \hologo{LaTeX} sources with \hologo{pdfLaTeX},
i.\,e.~the program \prog{pdflatex}.
%The \hologo{LaTeX} source tree can be downloaded as a zipped archive from the following address:
%\newline
%\makebox[\textwidth][c]{% a way of making some short stuff centered horizontally
%   \url{http://jsbsim.sourceforge.net/JSBSim_Reference_Manual_src.zip}
%}
The \hologo{LaTeX} source tree can be downloaded as a zipped archive from the above address.
On the navigation bar, on the right, besides
``RSS'', ``fork'' and ``follow'' icons
you can find a ``get the source'' pop-down menu. Choose the archive type and download.

The source code can also be retrieved from the revision management storage location using a software
tool called \prog{git} (similar to \prog{cvs}, Concurrent Versions System, CVS).
The program \prog{git} is available as an optional tool in the Cygwin environment under Windows, but
is usually standard with Linux systems.
The JSBSim manual \hologo{LaTeX} sources and related files (such as illustrations and screenshots
included in the final document) can be downloaded using \prog{git} as follows:
%
%\smallskip
\begin{asparaenum}
\item
Make sure you have \prog{git} installed.
Make a working directory on your system where you intend to download the source tree.
Open a terminal window and move to that directory.

\item
Give the following command:
%\begin{myCommandLine}
%$ »{\color{red!60!black}\textbf{cvs}}» \ »{\color{blue}\meta{enter}}»
%   -d:pserver:anonymous@jsbsim.cvs.sourceforge.net:/cvsroot/jsbsim \ »{\color{blue}\meta{enter}}»
%   login »{\color{blue}\meta{enter}}»
%   »{\color{blue}\meta{enter} \textrm{(when prompted for a password)}}»
%$ »{\color{red!60!black}\textbf{cvs}}» -z3 \ »{\color{blue}\meta{enter}}»
%   -d:pserver:anonymous@jsbsim.cvs.sourceforge.net:/cvsroot/jsbsim \ »{\color{blue}\meta{enter}}»
%   co -P JSBSim_Reference_Manual »{\color{blue}\meta{enter}}»
%\end{myCommandLine}
%
\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{git}}» »{\color{green!40!black}clone}» \
   https://bitbucket.org/jsbsim_manual_team/jsbsim_reference_manual.git
\end{myCommandLine}
%
%\smallskip
%\TODOInline[ADM]{Adjust this part as the repository name is frozen.}
\end{asparaenum}

\smallskip

When the download is complete you will have the whole source tree under your working directory.

With \prog{git} you can use either secure hypertext transport protocol (HTTPS) or
secure shell (SSH) to connect to Bitbucket.
HTTPS requires you to enter a username/password each time you connect to the Bitbucket server, for example, when you push your changes. HTTPS is suitable for situations where you work with Bitbucket infrequently and make few code changes.
The command given above uses HTTPS.

However, if you do most of your coding with a Bitbucket hosted repository, you'll want to set up a SSH connection. If this is the case you have to clone the repository by using the command:
\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{git}}» »{\color{green!40!black}clone}» \
   git@bitbucket.org:jsbsim_manual_team/jsbsim_reference_manual.git
\end{myCommandLine}

\smallskip
%----------------------------------------------------------------------------------
\noindent
\begin{minipage}{\linewidth}\small%\scriptsize
%\setstretch{0.9}
%\rule{0pt}{0.9em}
{\color{gray}\hrule}
\rule[0pt]{0pt}{0.9\baselineskip}% <=== SPACER
%**********************************************************************************
\ding{42}\ You can find more about managing Bitbucket repositories by visiting this page:
\newline
\adjustbox{center}{%
   \href{https://confluence.atlassian.com/display/BITBUCKET/bitbucket+Documentation+Home}%
        {\relsize{-1}\ttfamily https://confluence.atlassian.com/display/BITBUCKET/bitbucket+Documentation+Home}
}
%**********************************************************************************
%\rule[-0.4\baselineskip]{0pt}{0.9em}% <=== SPACER
{\color{gray}\hrule}
\end{minipage}
%\setstretch{\mynormalstretch}
%----------------------------------------------------------------------------------

\section{Getting a \TeX{} distribution}

Those who are new to \hologo{LaTeX} should know that the whole set of tools used to typeset documents in the \hologo{LaTeX}
markup language is called a ``\TeX{} system''. \TeX{} was the original language and also the name of the typesetting
system born in 1992 from the mind of the great computer scientist Donald Knuth
(\url{http://www-cs-faculty.stanford.edu/~uno/}). 

\smallskip

Nowadays there are two main freeware distributions of a \TeX{} system: 
\begin{compactenum}
\item \hologo{TeX}~Live, which is multi-platform and may be installed on Windows, Linux and Mac (the Mac version is named Mac\hologo{TeX}, but it is the same as \hologo{TeX}~Live plus some Mac specific applications), see
\url{http://www.tug.org/texlive/} (and \url{http://www.tug.org/mactex});
\item \hologo{MiKTeX}, for Windows only, see \url{http://miktex.org/}.
\end{compactenum}
These distributions are generally complete, up to date and upgradable almost daily on the internet.
They can be setup on the target operating sistem as a \emph{basic} installation (extendable on the fly according to the
typesetting needs) or as a \emph{complete installation}.

Although you may be tempted to choose a basic installation to save disk space,
in both cases it is adviced to go for a complete installation of the \TeX{} system.
With the latter you have everything available even when you are off line, while, even if the basic installation
may download new requested packages on the fly, you need to be on line.
And more importantly, having a complete installation ensures that all the required packages, fonts and utility
programs are already in place when compiling the sources of JSBSim manual.

\smallskip

For many reasons it might be better that Windows users neglect the \hologo{MiKTeX} distribution and install \hologo{TeX}~Live
(ver.~2012 or later),
which is considered the ``official'' distribution of \hologo{TeX}.
However, st the time of writing this guidelines the JSBSim manual compiles equally well with both distributions on Windows.

In order to install \hologo{TeX}~Live on Linux platforms it is necessary to distinguish between ``normal'' Linux and Linux/Debian.
%
For ``normal'' Linux it suffices to download from the internet page \url{http://mirror.ctan.org/systems/texlive/tlnet/} the installer file \file{install-tl-unx.tar.gz};
after decompressing read the instructions and proceed as indicated. But make sure you chose a reliable mirror.

Due to the variety of Linux platforms the standard \prog{TeXworks} editor is not automatically installed; very often the specific Linux package manager can supply an alternative means of installation, otherwise there is also the \prog{TeXworks} site where to execute the download from: \url{http://www.tug.org/texworks/#Getting_TeXworks}; if a binary download is not available for a specific Linux platform it is always possible to download the source files from: \url{http://code.google.com/p/texworks/downloads/list?can=3&q=Type%3DSource} 
and then proceed with the usual \prog{configure}, \prog{Make}, \prog{Make install} procedure. 

On Linux/Debian or GNU/Linux platforms a \TeX\ distribution might be already installed or it might be fetched and installed with the local package manager. Some Linux platforms, such as the Slackware one, by the end of 2011 still install the obsolete te\TeX\ distribution, that has been superseded by \TeX\ Live, and is not under maintenance any more. Other distributions, such as the GNU/Ubuntu one, are subject to the restrictions of the Debian Consortium and the \TeX~Live version available through the
local package manager might be two or three years old compared to the official TUG (\TeX{} Users Group, \url{www.tug.org})
controlled \TeX~Live one.
There are workarounds to this situation that can be found on specialized forums. For instance one can google for the string
``installing TeX Live on Ubuntu''.

On Mac OS X the installation of Mac\TeX{} is quite smooth and usually, once the complete installation is done, one is ready
to go and compile the JSBSim manual successfully.

If you already have a \hologo{TeX} system installed on your computer make sure it is 
\hologo{TeX}~Live 2012 or later, or \hologo{MiKTeX} 2.9 or later. If you have previous versions installed consider
switching to the most recent versions.
Moreover, make sure you have all the packages and utilities updated; this is necessary because the JSBSim manual sources
use some features provided by recent releases of \hologo{LaTeX} extension packages.
With a \hologo{TeX}~Live distribution you have the utility program \prog{tlmgr} (\hologo{TeX}~Live manager) that serves
for all configuration tasks. To update completely your local \hologo{TeX}~Live installation you just do the following
from the command line:

\smallskip

%----------------------------------------------------------------------------------
%% See file "_lst_defs.tex".
%% [escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{tlmgr}}» --all --self »{\color{blue}\meta{enter}}»
\end{myCommandLine}
%----------------------------------------------------------------------------------

\smallskip

Both \hologo{TeX}~Live and \hologo{MiKTeX} also provide GUI-based utilities that help to configure and update
the \hologo{TeX} system.

\section{Working with a \TeX{} system}

\subsection{Basics}

\hologo{LaTeX} \citep{latexbook} was developed by Leslie Lamport as an extension of Donald Knuth’s \hologo{TeX} macro-based
programming language understood by the \prog{tex} typesetting program.
(\url{http://en.wikipedia.org/wiki/TeX}). 
It consists of a Turing-complete procedural markup language and a typesetting processor.
The combination of the two lets you control both the visual presentation as well as the content of your documents.

\smallskip

The following three steps explain how you use \hologo{LaTeX}:
\begin{compactenum}
\item
You write your document in a \hologo{LaTeX} (\verb!.tex!) input (source) file.
\item
You run the \prog{latex} or \prog{pdflatex} program on your input file. 
This turns the input file into a DeVice Independent (DVI, \verb!.dvi!) file or a Portable Document Format
(PDF, \verb!.pdf!) file. 
This process is loosely called ``compilation'' of the source.
If there are errors in your source file then you should fix them before you can continue to
next step.
The JSBSim manual is designed to be compiled with \prog{pdflatex} and the output is the file
\verb!JSBSim_Reference_Manual.pdf!.
\item
You view the DVI or PDF file on your computer. A DVI file cannot be printed directly.
You have to convert it to Postscript or PDF and then print the result of the conversion.
The advantage of PDF is that it can be printed straight away.
This is the case of the JSBSim manual, which is compiled directly into a PDF file.
\end{compactenum}

\smallskip

Roughly speaking \hologo{LaTeX} is built on top of \TeX{}. This adds extra functionality to \TeX{} and makes writing
your document much easier. With \hologo{LaTeX} being built on top of \TeX{}, the result is a \TeX{} program.

\subsection{Editing your sources}

To work with your \hologo{LaTeX} sources, once you have installed correctly a complete \TeX{}~Live
or MiK\TeX{} distribution, you have the program \prog{\TeX{}works} (\url{http://www.tug.org/texworks/}).
This is a simple \TeX{} front-end program (a working environment) and you use it as a \hologo{LaTeX}-oriented integrated
development environment (IDE). \prog{\TeX{}works} is recommended especially for those who are new to \hologo{LaTeX}.

A manual for \prog{\TeX{}works} is included in the \TeX{} distribution automatically. It is quite extensive and should provide
all the necessary information for normal usage. This manual normally is accessible from the ``Help'' menu of the application
(and possibly also from other locations, such as the Microsoft Windows start menu).
The latest version is also available on Google Code at: \url{http://code.google.com/p/texworks/issues/detail?id=261}\,.

All the \verb!.tex! files used to typeset the JSBSim manual are text files encoded according to
UTF-8 (\url{http://en.wikipedia.org/wiki/UTF-8}). Therefore, make sure this encoding is properly set through the
\prog{\hologo{TeX}woks} ``Preferences'' menu.

If you are not a newbie of \hologo{LaTeX} and you know how to work with sources you probably know other
\hologo{LaTeX}-dedicated IDEs, such as \prog{Kile} for Linux, \prog{WinEdt} for Windows, \prog{\hologo{TeX}shop} for Mac,
or \prog{\hologo{TeX}clipse} based on the Eclipse framework. Still, \prog{\hologo{TeX}works} is a good option also for
``\hologo{TeX}perts.''
To have an idea of the many IDEs available you can visit this link:
\url{http://tex.stackexchange.com/questions/339/latex-editors-ides}\,.


\section{Typesetting the JSBSim manual}

\subsection{Using the command line}

The way we produce the final PDF of the JSBSim manual is readily understood when we look at the command line
procedure necessary to accomplish the task.
To do so just change directory and position yourself where your local copy of the manual sources resides.
We will call this path \meta{JSBSim manual root}.
Now simply give the commands:

\smallskip

%----------------------------------------------------------------------------------
%% See file "_lst_defs.tex".
%% [escapechar=»,escapebegin=\color{blue}\relsize{0}\rmfamily,escapeend={}]
\begin{myCommandLine}
$ »{\color{red!60!black}\textbf{pdflatex}}» JSBSim_Reference_Manual »{\color{blue}\meta{enter}}»
   »{\color{gray}\rmfamily\ldots\ a long sequence of log messages generated by \prog{pdflatex}\ \ldots}»
$ »{\color{red!60!black}\textbf{biber}}» JSBSim_Reference_Manual »{\color{blue}\meta{enter}}»
   »{\color{gray}\rmfamily\ldots\ a sequence of log messages generated by \prog{bibtex}\ldots}»
$ »{\color{red!60!black}\textbf{pdflatex}}» JSBSim_Reference_Manual »{\color{blue}\meta{enter}}»
   »{\color{gray}\rmfamily\ldots\ a long sequence of log messages generated by \prog{pdflatex}\ \ldots}»
$ »{\color{red!60!black}\textbf{pdflatex}}» JSBSim_Reference_Manual »{\color{blue}\meta{enter}}»
   »{\color{gray}\rmfamily\ldots\ a long sequence of log messages generated by \prog{pdflatex}\ \ldots}»
\end{myCommandLine}
%----------------------------------------------------------------------------------

\smallskip

The first of the four above commands runs the ``first pass of \hologo{LaTeX}'' over the main source file
\verb!JSBSim_Reference_Manual.tex!\,.
Actually, as you see, we are using the program \prog{pdflatex}
so we should use the word \hologo{pdfLaTeX}. 
But here we follow the common way of speaking about this subject, so in the rest of these guidelines when we will be talking of
``compilation with \hologo{LaTeX}'' we will mean ``typesetting with \prog{pdflatex}.''

The first \hologo{LaTeX} pass already produces the file \verb!JSBSim_Reference_Manual.pdf!\,.
If your local copy of the manual sources is a fresh download from the JSBSim repository, probably
this PDF file should have consistent cross-references to chapters
and sections, tables, figures, and so on, and correct bibliographic citations. 
If you have changed the sources --- for instance by adding text, or by changing the order of two figures,
or you included a new bibliographic source --- you need some extra passes to get a correctly typeset final document.

The second command above runs the program
% \prog{bibtex} 
\prog{biber} 
over the main source file.
This is called the ``\hologo{BibTeX} pass'' (well, actually the ``\prog{biber} pass'').
The program \prog{biber}
is used for the automatic management of
bibliographic citations inside the main document. 
Nowadays \prog{biber} replaces a similar program called \prog{bibtex} that gave the name to the 
bibliographic database format.
\prog{biber} processes some auxiliary files produced by a previous
\hologo{LaTeX} pass and creates another auxiliary file needed by subsequent \hologo{LaTeX} passes.
This step serves for correct automatic formatting of citations.

The \hologo{LaTeX} auxiliary files have various extensions (\verb!.aux!, \verb!.out!, \verb!.idx!, \verb!.ind!, etc).
They are used by \prog{pdflatex} itself and by \prog{biber}, but are not to be edited by users.
One file that sometimes is worth checking is the \verb!.log! file that contains all the output messages issued during a 
compilation pass.

The third and fourth commands of the above sequence are simply two extra \hologo{LaTeX} passes that serve to finalize the automatic
citation and cross-referencing mechanism and to get the right positioning of figures.
At the end of the process you should have the correct version of the file \verb!JSBSim_Reference_Manual.pdf!\,.

\smallskip
%----------------------------------------------------------------------------------
\noindent
\begin{minipage}{\linewidth}\small%\scriptsize
%\setstretch{0.9}
%\rule{0pt}{0.9em}
{\color{gray}\hrule}
\rule[0pt]{0pt}{0.9\baselineskip}% <=== SPACER
%**********************************************************************************
\ding{42}\ The JSBSim manual layout is enginered in such a way that a correct placement of
page decorations need more than two extra \hologo{LaTeX} passes. Usually, when working at the manual
users end up compiling the source several times; this ensures that all the various graphic elements
go in the right place.
%**********************************************************************************
\rule[-0.4\baselineskip]{0pt}{0.9em}% <=== SPACER
{\color{gray}\hrule}
\end{minipage}
%\setstretch{\mynormalstretch}
%----------------------------------------------------------------------------------
%\smallskip

\subsection{Using a \TeX{} front-end}

%---------------------------------------------------------------------------------
%\begin{figure}[t]
%   \centering
%   \includegraphics[width=1.10\textwidth]{images/TeXworks_screenshot.png}
%   \caption{Editing a source file with \prog{\TeX{}works} editor (left pane) and viewing the PDF at the same time (right pane).}
%   \label{fig:TeXworks:Session}
%\end{figure}
\EnlargedFigure% needs two latex passes
    {t}% #1 <where>: t, b, p
    {images/TeXworks_screenshot.png}% #2 <file>: the image file included by \includegraphics
    {width=\linewidth}% #3 <opt-list>: option list to pass to \includegraphics, e.g. width=\linewidth,rotate=0
    {A screenshot of a \prog{\TeX{}works} working session while the JSBSim manual is being compiled. 
     A source file is loaded in the \prog{\TeX{}works} editor (left pane) and the PDF of the manual
     if visualized by the application previewer (right pane). The lower part of the editor shows the log
     messages given by \prog{pdflatex} during a \hologo{LaTeX} pass; this is closed autamatically when compilations
     end successfully.
     When an error occurs the log windows becomes interactive and user can input some additional commands.
    }% #4 <caption>: the caption text
    {fig:TeXworks:Session}% #5 <label>: the label
%---------------------------------------------------------------------------------

The same sequence of compilation passes discussed above is accomplished with minimum effort within
a \prog{\TeX{}works} working session, see Figure~\vref{fig:TeXworks:Session}.
To do so open \prog{\TeX{}works} and load the file \verb!JSBSim_Reference_Manual.tex!\,.
If you explore the source with the editor you find that the first few lines of this text file look like
the following:

\smallskip

%---------------------------------------------------------------------------------
\input{_lst_reset}
\input{_lst_latex}
\begin{lstlisting}[%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\color{colComments}\ttfamily\small,%\color{blue}\relsize{0}\rmfamily,%
    escapeend={}]
%*****************************************************
%
% AUTHOR:      »Agostino De Marco», Jon S. Berndt,
%              and the JSBSim Development Team
% DESCRIPTION: This is "JSBSimReferenceManual.tex", 
%              the master source file of
%              JSBSim Reference Manual.
%              When compiled with »pdflatex» creates a
%              PDF document of the manual.
% USAGE:       Use the following command line:
%              »\$ pfdlatex JSBSimReferenceManual»
%
%*****************************************************

%-----------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TEX encoding = UTF-8
% !TEX program = »pdflatex»
%-----------------------------------------------------

%-----------------------------------------------------
% INITIAL DEFINITIONS
%-----------------------------------------------------

\input{_init}% plain TeX initial definitions in »./\textunderscore{}init.tex»

%-----------------------------------------------------
% LaTeX STARTING DECLARATION
%-----------------------------------------------------

\documentclass[»\bfseries\color{mydarkgreen}12pt,twoside»]{»\bfseries\color{colEnv}book»}

...
\end{lstlisting}
%---------------------------------------------------------------------------------

You realize easily that the character `\texttt{\%}' is used in \hologo{TeX} and \hologo{LaTeX} to
comment out the rest of the line.
Moreover, you realize that the above snippet contains the \hologo{LaTeX} keyword \verb!\documentclass!,
which is the opening declaration of a \hologo{LaTeX} document. A source file that contains this command is called
the ``main file.''

A document is typeset in \prog{\TeX{}works} by opening the main file and clicking on the ``Typeset'' button
(the green triangular icon at the top left of the editor pane). As an alternative, the keyboard shortcut that
runs a \hologo{LaTeX} pass is \textsf{Ctrl+T} (\textsf{Cmd+T} on Mac).
During compilations the ``Typeset'' button becomes temporarily red as in Figure~\ref{fig:TeXworks:Session}.
Keep in mind that the JSBSim manual is typeset with \prog{pdflatex}, so make sure you have ``pdflatex''
selected in the drop-down list near the ``Typeset'' button.

\prog{\TeX{}works} is a very simple and intuitive software tool. When a document is compiled
the lower part of the editor becomes a log message window that displays all the messages issued by the typesetter
program (in our case \prog{pdflatex}). These are the same messages that eventually are written in the \verb!.log! file.
When a compilation ends successfully the log window closes autamatically and the
\prog{\TeX{}works} previewer window takes the focus displaying the PDF just produced.

\prog{\TeX{}works} provides two very useful navigation features known as ``forward search'' and ``inverse search''.
The inverse search is the one that makes nowadays working with \hologo{LaTeX} much less tedious to new users than
it was in the past: You navigate through your PDF file with the \prog{\hologo{TeX}works} previewer; when you
want to check which part of your \hologo{LaTeX} code has produced a specific part of the output, you just
\textsf{Ctrl+click} on that point in the previewer (\textsf{Cmd+click} on Mac); then the source editor pane 
takes the focus and you jump directly to the corresponding place in the source.
This feature speeds up tremendously your work.

The direct search is similar to the inverse search: You \textsf{right click} on some text in the source editor;
the pop-up menu tells you ``Go to PDF''; you click on it;
the previewer pane takes the focus and displays the part of the PDF file that corresponds to the
point in \hologo{LaTeX} code you clicked on.

\section{Getting support}

To get support on the compilation of the manual simply ask for help on the JSBSim mailing list.
To know more about \TeX{}/\hologo{LaTeX} visit these web links:
\begin{compactitem}
\item \url{http://tug.org}
\item \url{http://tex.stackexchange.com}
\item \url{http://www.latex-community.org}
\end{compactitem}

%% EOF