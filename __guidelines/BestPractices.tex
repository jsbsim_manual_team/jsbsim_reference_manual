%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ./TypesettingGuidelines_JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
\chapter{Best practices when adding contents}\label{ch:Best:Practices}

Readers are warned that creating well-designed documents is about more than the technical
detail of any typesetting system, and so as well as learning \hologo{LaTeX} it is also necessary to understand 
the wider ideas of good writing and good design if one is to create truly `beautiful' material.

\section{Text}

Type your text in the source files as if you were writing a letter, made of plain ASCII characters.

Sometimes you will need to put some extra attention here and there whenever a special typographic element is needed.
For example, when you want to emphasize a word, for some reason, typically you use an italic style as in the
following code snippet:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
JSBSim is a software \emph{library}, not exactly a program.
\end{tcblisting}

\medskip
\noindent
This example shows both the \hologo{LaTeX} code (the upper part of the frame) and the typeset output
(the lower part of the frame).
The command \cs{emph}\marg{argument} emphasizes its argument; hence the word ``library'' is typeset in italics,
which is the default way a chunk of text is emphasized.
If you want to obtain an italicized word or chunk of text you can use also the command
\cs{textit}\marg{some text}.

Just keep in mind that the \hologo{LaTeX} compiler gives a special meaning to a finite number of so-called
\emph{special characters}.
We have already seen the start-of-comment token \verb!%!;
if you want to typeset right that character, `\%', you have
to use the command \cs{\%}.
Then, there is the backslash \verb!\!, which starts \hologo{LaTeX} control sequences. 
Typesetting a backslash, `\textbackslash', is done with the commands \cs{textbackslash}
(and \cs{backslash} in mathematical formulas).
The character \verb!#! has a special meaning and is used in low-level \hologo{LaTeX} programming;
it is typeset as `\#' in normal text with the command \cs{\#}.
The ampersand character \verb!&! is called ``alignment tab'' by \hologo{LaTeX} users because it indicates a horizontal
alignment position in array-like structures consisting of rows and columns. If you want to typeset the ampersand,
`\&' you have to use the command \cs{\&}.
The math mode switch token \verb!$!, the subscript token \verb!_!, 
and the exponent token \verb!^! are used in math formulas.
Respectively, they are typeset in normal text as `\$' with the command \cs{\$},
as `\_' with the command \cs{\_}, and as `\textasciicircum' with the command \cs{textasciicircum}.

Single and double quote marks are available as \emph{opening} and \emph{closing} quotes as follows:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
The character `\$' is special. 
It starts and closes an ``inline math formula.''
\end{tcblisting}

\medskip\noindent
Note the use of backticks (\verb!`!) as opposed to normal tickmarks (\verb!'!).

\medskip

Another typographic structure often needed in reports and technical documents is the list.
Here is an example of unordered list:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
Having said that, I feel I need:
\begin{itemize}
   \item some pizza,
   \item an iced cappuccino,
   \item one donut.
\end{itemize}
Then I'll consider myself full.
\end{tcblisting}

\medskip\noindent
For the JSBSim manual we use a compact variant of the standard \verb!itemize! environment,
named \verb!compactitem!. Here is the above example retyped:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
Having said that, I feel I need:
\begin{compactitem}
   \item some pizza,
   \item an iced cappuccino,
   \item one donut.
\end{compactitem}
Then I'll consider myself full.
\end{tcblisting}

\medskip
Similarly, for numbered lists we prefer using the environment \verb!compactenum! rather than
the standard \verb!enumerate! environment. So we may go:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
Having said that, I feel I need:
\begin{compactenum}
   \item some pizza,
   \item an iced cappuccino,
   \item one donut.
\end{compactenum}
Then I'll consider myself full.
\end{tcblisting}


\section{Bibliographic citations}

To insert bibliographic citations in your document you need to prepare a \verb!.bib! file.
For this task one can use the program \prog{Jabref}.
Each item in the bibliographic database has its own label, for example \verb!Tufte2001!.
The following example shows a simple way to handle a bibliographic citation in your text:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
One should read Tufte's book \citep{Tufte2001} almost
two times; it is very interesting.
\end{tcblisting}

\medskip\noindent
The command \cs{citep} inserts the reference in parentheses. The reference is listed in the bibliography which
appears typically at the end of the document.
Consider that the bibliographic citations may follow a certain \emph{citation style}. In this document the
bibliographic database is printed as and numbered list and each item has its own number.

Here are other possible ways to handle the same citation:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
According to \citet{Tufte2001}, we need to present the data
as clearly as possible.
\citeauthor{Tufte2001}, the author of \citetitle{Tufte2001},
claims that data need to be presented in a clear way.
As said elsewhere \citep[see][p.~251]{Tufte2001}
data need to be presented in a clear way.
\end{tcblisting}

\medskip\noindent
Note the use of the alternative citation commands:
\cs{citet}, wich stands for ``cite as running text'',
\cs{citeauthor}, which extract the author from the database record,
\cs{citetitle}, which takes the title, and \cs{citep}\oarg{pre-text}\oarg{post-text}\marg{label}, which prepends
and appends some text to the reference number cited in parentheses.

\section{Code fragments}

\lipsum[1]

\section{Math}

When it comes to typesetting mathematical symbols one should be aware of some
typographical conventions.
These consist in a set of rules, which should be adhered to not only in scientific manuscripts,
but even in informal reports. With modern text editors there is no excuse not to use roman, italic
or bold type when required. The rules are simple:
\begin{compactitem}
\item italic type for scalar quantities and variables,\\
Example: ``$V$ is the airspeed.''
\item roman type for units, prefixes, and sub/superscripts (mind capitalization),\\
Example: ``$V=\SI{190}{\meter/\second}$,'' or ``$V_\mathrm{TAS}=\SI{190}{\meter/\second}$.''
\item italic boldface for a vector or matrix quantity,\\
Example: ``The velocity vector is $\boldsymbol{V}$.''
\item sans-serif bold italic for tensors,\\
Example: ``The transformation matrix {\sffamily\bfseries\itshape R}.''
\item roman type for chemical elements and other descriptive terms, including
mathematical constants, functions and operators,\\
Example: ``The math constant e,'' or ``the rate of climb is $\mathrm{RC}=V\sin\gamma$.''
\end{compactitem}

Following these rules with \hologo{LaTeX} is very easy.
Here is an example of inline math:

\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
Throughout this document $V$ is the airspeed.
Sometimes we use the symbole $V_{\mathrm{TAS}}$ to indicate
explicitly a \emph{True Airspeed}.
\end{tcblisting}

\medskip
In the above text we use the character \verb!$! to delimit a math expression wich runs with the text.
In other occasions we may need to typeset a ``displayed'' mathematical expression:


\medskip
\begin{tcblisting}{
   colback=red!5,colframe=red!75!black,
   listing options={basicstyle=\ttfamily\normalsize,
      backgroundcolor={}}
   }
The following expression defines the dynamic pressure
\begin{equation}
   \label{eq:Dynamic:Pressure}
   \bar{q} = \frac{1}{2} \rho V^2
\end{equation}
The formula (\ref{eq:Dynamic:Pressure}) will be applied
many times.
\end{tcblisting}

\medskip\noindent
The label.
\section{Some test material}

\lipsum[1]

%\tcbset{/tcb/boxsep=0pt}
Now some eamples taken from package \pkgname{tcolorbox}:
\begin{dispExample}
This is a \LaTeX\ example. See this formula: $\bar{q}=\dfrac{1}{2}\rho V^2$.
\end{dispExample}

This is a \cs{foocommand}.

In math mode the command \cs{dfrac}
is used to typeset a fraction.

\begin{tcolorbox}
This is a \textbf{tcolorbox}.
\end{tcolorbox}

Another one is:

\begin{tcolorbox}[colback=red!5,colframe=red!75!black,title={\bfseries My nice heading}]
This is another \textbf{tcolorbox}.
\tcblower
Here, you see the lower part of the box.
\end{tcolorbox}

Now an XML emample:
\begin{tcblisting}{
   colback=yellow!5,colframe=yellow!50!black,listing only,
   title=This is source code in another language (XML),
   fonttitle=\bfseries,
   listing options={language=XML,columns=fullflexible,
      basicstyle=\ttfamily\footnotesize,keywordstyle=\color{red}}}
<?xml version="1.0"?>
<project name="Package tcolorbox" default="documentation" basedir=".">
   <description>
      Apache Ant build file (http://ant.apache.org/)
   </description>
</project>
\end{tcblisting}

Now I want to test the package \textsf{glossaries}.
This is \gls{x:Body}. This is \gls{acr:AIAA}.
And we have \gls{acr:MST} and \gls{acr:GNC}.
This is a \gls{Motion:Platform}.

This is \gls{x:Stability}.

Or we have also \gls{acr:DAVE-ML}.

%\lipsum[1]

\TODOInline[ADM]{taken from Wikipedia.\newline
\url{http://en.wikipedia.org/wiki/Flight_sim}.}

Now some stuff for testing the index.
To solve various problems in physics, it can be advantageous
to express any arbitrary piecewise-smooth function as a Fourier Series
\index{Fourier Series}
composed of multiples of sine and cosine functions.


%% EOF