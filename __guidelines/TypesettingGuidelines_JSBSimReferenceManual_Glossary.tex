%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ./TypesettingGuidelines_JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%

%--------------------------------------------------------------------------------
%                                                G L O S S A R Y    E N T R I E S

%%% ---------------------------------------------------------------- Main glossary

\newglossaryentry{Motion:Platform}{%
   name=motion platform,
   description={a device used in flight simulators to give an acceleration feedback}
}

\newglossaryentry{Magneto}{%
   name=magneto,
   description={%
      a device that creates the high voltages required for aircraft engine spark plugs. 
      It combines the functions of an automobile engine's coil and distributor}
}

\newglossaryentry{Body:Frame}{%
   name=body frame,
   description={%
      a standard aircraft body-fixed frame, with origin at center of mass, $x$-axis pointing forward,
      $y$-axis pointing towards the right wing, $z$-axis positively oriented from head to feet of the pilot}
}

\newglossaryentry{Stability:Frame}{%
   name=stability frame,
   description={%
      a frame with origin at aircraft center of mass, where the $x$-axis is taken by projecting
      the relative wind vector onto the $xy$ plane of symmetry for the aircraft,
      the $y$-axis points out the right wing (coincides with the body $y$-axis),
      the $z$-axis completes the right-hand system.
      In dynamic stability studies this is considered a particular kind of body-fixed frame,
      often defined with respect to an initial symmetrical, steady, wings-level, constant
      altitude flight condition, that gives the direction of $x_\Stability$}
}

\newglossaryentry{Structural:Frame}{%
   name=structural frame,
   description={%
      a common manufacturer’s frame of reference, used to define points on the aircraft
      such as the center of gravity, the locations of all the wheels, the pilot eye-point, 
      point masses, thrusters, etc}
}

\newglossaryentry{Wind:Frame}{%
   name=wind frame,
   description={%
      similar to the stability frame, except that the $x$-axis points directly into the relative wind;
      the $z$-axis is perpendicular to the $z$-axis, and remains within the aircraft plane of symmetry
      (i.e.~body axis $xz$-plane, also called the reference plane); the $y$-axis completes the right
      hand coordinate system}
}

%%% --------------------------------------------------------------- List of symbols
%%% see _local_macros.tex

\newglossaryentry{alpha:Body}{%
   type=symbols,
   name={\ensuremath{\alpha_\Body}},
   sort=zz:alpha,
   description={angle of attack referred to $x$-body axis}
}

\newglossaryentry{beta}{%
   type=symbols,
   name={\ensuremath{\beta}},
   sort=zz:beta,
   description={angle of sideslip}
}

\newglossaryentry{C:Body:To:Earth}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{C}_{\Body/\earth}}},
   sort=C:BtoE,
   description={transformation matrix from ECEF to body}
}

\newglossaryentry{Drag}{%
   type=symbols,
   name={\ensuremath{D}},
   sort=D,
   description={aerodynamic drag}
}

\newglossaryentry{Lift}{%
   type=symbols,
   name={\ensuremath{L}},
   sort=L,
   description={aerodynamic lift}
}

\newglossaryentry{omega:Body:To:Vertical}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Body/\Vertical}}},
   sort=zz:omega:BtoV,
   description={angular velocity vector of the body-fixed frame relative to the vehicle NED (local) frame}
}
\newglossaryentry{omega:Vertical:To:Earth}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Vertical/\earth}}},
   sort=zz:omega:VtoE,
   description={angular velocity vector of the vehicle NED (local) frame relative to the ECEF frame}
}
\newglossaryentry{omega:Body:To:Earth}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Body/\earth}}},
   sort=zz:omega:BtoE,
   description={angular velocity vector of the body-fixed frame relative to the ECEF frame}
}
\newglossaryentry{omega:Body:To:Earth:Body}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Body/\earth}^\Body}},
   sort=zz:omega:BtoE:B,
   description={angular velocity vector of the body-fixed frame relative to the ECEF frame, 
    expressed in the body-fixed frame}
}
\newglossaryentry{omega:Body:To:Inertial}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Body/\Inertial}}},
   sort=zz:omega:BtoI,
   description={angular velocity vector of the body-fixed frame relative to the ECI frame}
}
\newglossaryentry{omega:Body:To:Inertial:Body}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\Body/\Inertial}^\Body}},
   sort=zz:omega:BtoI:B,
   description={angular velocity vector of the body-fixed frame relative to the ECI frame, 
    expressed in the body-fixed frame}
}
\newglossaryentry{omega:Earth:To:Inertial}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\earth/\Inertial}}},
   sort=zz:omega:EtoI,
   description={angular velocity vector of the ECEF frame relative to the ECI frame}
}
\newglossaryentry{omega:Earth:To:Inertial:Body}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{\omega}_{\earth/\Inertial}^\Body}},
   sort=zz:omega:EtoI:B,
   description={angular velocity vector of the ECEF frame relative to the ECI frame, 
    expressed in the body-fixed frame}
}
\newglossaryentry{V:Body:To:Earth}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{v}_{\Body/\earth}}},
   sort=Vel:BtoE,
   description={body-fixed frame velocity vector of the vehicle relative to the ECEF frame}
}
\newglossaryentry{V:Body:To:Earth:Body}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{v}_{\Body/\earth}^\Body}},
   sort=Vel:BtoE:B,
   description={body-fixed frame velocity vector of the vehicle relative to the ECEF frame, expressed
    in the body-fixed frame}
}
\newglossaryentry{V:Body:To:Earth:Vertical}{%
   type=symbols,
   name={\ensuremath{\boldsymbol{v}_{\Body/\earth}^\Vertical}},
   sort=Vel:BtoE:V,
   description={body-fixed frame velocity vector of the vehicle relative to the ECEF frame, expressed
    in the vehicle-carried NED (local) frame}
}

\newglossaryentry{var:Ti2ec}{%
   type=symbols,
   name={\mdseries\texttt{Ti2ec}},
   sort=Ti2ec,
   description={variable of class \texttt{FGMatrix33}, a 3$\times$3 matrix implementing
   the ECI (inertial) to ECEF frame transformation matrix}
}

\newglossaryentry{var:Tec2i}{%
   type=symbols,
   name={\mdseries\texttt{Tec2i}},
   sort=Ti2ec,
   description={variable of class \texttt{FGMatrix33}, a 3$\times$3 matrix implementing
   the ECEF to ECI (inertial) frame transformation matrix (transpose of \texttt{Ti2ec})}
}

\newglossaryentry{var:vUVW}{%
   type=symbols,
   name={\mdseries\texttt{vUVW}},
   sort=vUVW,
   description={variable of class \texttt{FGVector}, a 3-element column implementing
   the body-fixed frame velocity vector of the vehicle relative to the ECEF frame,
   expressed in the body-fixed frame, $\boldsymbol{v}_{\Body/\earth}^\Body$}
}
\newglossaryentry{var:vVel}{%
   type=symbols,
   name={\mdseries\texttt{vVel}},
   sort=vVel,
   description={variable of class \texttt{FGVector}, a 3-element column implementing
   the body-fixed frame velocity vector of the vehicle relative to the ECEF frame, expressed
    in the vehicle-carried NED (local) frame, $\boldsymbol{v}_{\Body/\earth}^\Vertical$}
}
\newglossaryentry{var:vPQR}{%
   type=symbols,
   name={\mdseries\texttt{vPQR}},
   sort=vPQR,
   description={variable of class \texttt{FGVector}, a 3-element column implementing
   the body-fixed frame angular velocity vector of the vehicle relative to the ECEF frame, expressed
    in the body frame, $\boldsymbol{\omega}_{\Body/\earth}^\Body$}
}
\newglossaryentry{var:vPQRi}{%
   type=symbols,
   name={\mdseries\texttt{vPQRi}},
   sort=vPQRi,
   description={variable of class \texttt{FGVector}, a 3-element column implementing
   the body-fixed frame angular velocity vector of the vehicle relative to the ECEF frame, expressed
    in the body frame, $\boldsymbol{\omega}_{\Body/\Inertial}^\Body$}
}

\newglossaryentry{x:Body}{%
   type=symbols,%                      goes in the list of symbols
   name={\ensuremath{x_\Body}},
   sort=xB,
   description={aircraft body-fixed longitudinal (roll) axis}
}

\newglossaryentry{x:Stability}{%
   type=symbols,
   name={\ensuremath{x_\Stability}},
   sort=xS,
   description={aircraft body-fixed $x$-stability (roll) axis}
}

\newglossaryentry{YA}{%
   type=symbols,
   name={\ensuremath{Y_\Aero}},
   sort=YA,
   description={aerodynamic side force}
}

\newglossaryentry{y:Body}{%
   type=symbols,
   name={\ensuremath{y_\Body}},
   sort=yB,
   description={aircraft body-fixed lateral (pitch) axis}
}

\newglossaryentry{z:Body}{%
   type=symbols,
   name={\ensuremath{z_\Body}},
   sort=zB,
   description={aircraft body-fixed normal (yaw) axis}
}

\newglossaryentry{z:Vertical}{%
   type=symbols,
   name={\ensuremath{z_\Vertical}},
   sort=zV,
   description={local vertical axis, with origin in aircraft center of mass, pointing downwards}
}

%%% -------------------------------------------------------------------- Acronyms

\newacronym{acr:AGL}{AGL}{%
   Above Ground Level.\glspar
   Used to describe an aircraft's altitude above the ground.
   Aircraft instruments do not provide the pilot with this information. 
   Altitude AGL must be deduced from a reading of the altimeter and charts or
   knowledge of the terrain over which the aircraft is operating
}
\newacronym{acr:AIAA}{AIAA}{American Institute of Aeronautics and Astronautics}
\newacronym{acr:DAVE-ML}{DAVE-ML}{Dynamic Aerospace Vehicle Exchange Markup Language}
\newacronym{acr:DME}{DME}{%
   Distance Measuring Equipment.\glspar
   Provides the pilot with a panel readout of the aircraft's distance from a VOR station, in nautical miles
}
\newacronym{acr:FAA}{FAA}{Federal Aviation Administration}
\newacronym{acr:GNC}{GNC}{Guidance Navigation and Control}
\newacronym{acr:JSBSim-ML}{JSBSim-ML}{JSBSim Markup Language}
\newacronym{acr:MST}{MSTC}{AIAA Modelling and Simulation Technical Commitee}
