%------------------------------------------------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = ./TypesettingGuidelines_JSBSimReferenceManual.tex
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
\chapter{\LaTeX{} source organization}

\section{Exploring the main source file}

In previous chapter we have seen a snippet of the JSBSim manual main \hologo{LaTeX} file.
Now let us make a dissection of it by listing the fundamental parts in which the source is logically organized.

The first part of the main file starts from the first line, includes the line containing the command \verb!\documentaclass!
and terminates just before the line containing the command \verb!\begin{document}!.
This part of the file is usually called the ``preamble'' of the main source file. The JSBSim manual main \hologo{LaTeX}
file has the following preamble (all the inessential comment lines have been stripped off for brevity):

\smallskip

%---------------------------------------------------------------------------------
\input{_lst_reset}
\input{_lst_latex}
\begin{lstlisting}[%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%
    escapeend={}]
%                                  INITIAL DEFINITIONS
% plain TeX initial definitions in »\color{colComments}./\textunderscore{}init.tex»
\input{_init}
%                           LATEX STARTING DECLARATION
\documentclass[»\bfseries\color{colOpt}12pt,twoside»]{»\bfseries\color{colEnv}book»}
%---------------------------------------------PREAMBLE
% all preamble settings in »\color{colComments}./\textunderscore{}preamble.tex»
\input{_preamble}
%                    LOCALUSER-DEFINED MACROS/SETTINGS
% all local customizations in »\color{colComments}./\textunderscore{}local\textunderscore{}macros.tex»
\input{_local_macros}
%                                SETTINGS FOR GLOSSARY
% special settings for the glossary in »\color{colComments}./\textunderscore{}glossary.tex»
\input{_glossary}
»\adjustbox{right}{\rmfamily\color{gray}(to be continued)}»
\end{lstlisting}

\smallskip

As you can see the content of this preamble is mainly a sequence of logical markup commands.
The command \verb!\documentaclass! is a typical \hologo{LaTeX} markup. The backslash character `\verb!\!' is prepended to
the keyword ``\verb!documentaclass!'' --- in \hologo{TeX}/\hologo{LaTeX} language we say that \verb!\documentaclass!
is a ``macro.'' This macro accepts an argument and a list of configuration options: the argument is passed in curly brackets,
\verb!{!\meta{arg}\verb!}!, the options are passed as a list of comma-separated strings in square brackets,
\verb![!\meta{option-list}\verb!]!.
The command

\noindent
\adjustbox{center}{\verb!\documentclass[12pt,twoside]{book}!}

\noindent
says that
\begin{compactitem}
\item
the layout and style of the current document is that of a book (argument \verb!book!, selects a
predefined style),
\item
the font dimension of normal text is set at \SI{12}{pt} (option \verb!12pt!),
\item
the content of the document is printed both on \emph{recto} (front) and \emph{verso} (back) side of the page (option \verb!twoside!).
\end{compactitem}
There are many other macros in \hologo{LaTeX} but \verb!\documentclass! is a special one: it marks the beginning
of \hologo{LaTeX} code and there can be only one occurrence of it.
All lines of code allowed before this macro in the main source file are
%(most of the times)
not \hologo{LaTeX} markup.
They are
%(most of the times)
``plain \hologo{TeX}'' commands because they are processed at low level by the 
\hologo{TeX} typesetting engine.

\smallskip
%----------------------------------------------------------------------------------
\noindent
\begin{minipage}{\linewidth}\small%\scriptsize
%\setstretch{0.9}
%\rule{0pt}{0.9em}
{\color{gray}\hrule}
\rule[0pt]{0pt}{0.9\baselineskip}% <=== SPACER
%**********************************************************************************
\ding{42}\ Keep in mind that \hologo{LaTeX} is a language built on top of \hologo{TeX} so, even after \verb!\documentclass!,
one can always mix \hologo{LaTeX} markup commands with \hologo{TeX} primitive commands. But this is usually not necessary
and not recommended for non expert users.
%**********************************************************************************
\rule[-0.4\baselineskip]{0pt}{0.9em}% <=== SPACER
{\color{gray}\hrule}
\end{minipage}
%\setstretch{\mynormalstretch}
%----------------------------------------------------------------------------------
\smallskip

Another example of macro in the above preamble is \verb!\input!. This is originally a \hologo{TeX} primitive command
that has been redefined in \hologo{LaTeX} to comply with the language markup rules.
Each time an occurrence of the command 
\verb!\input{!\meta{file name}\verb!}!
is found, a file named ``\meta{file-name}\verb!.tex!'' is loaded and processed by the \hologo{LaTeX} markup interpreter
as if it was a portion of the calling code.
The external source is supposed to be a sequence of \hologo{LaTeX} comands, of plain \hologo{TeX} commands or a mix of them.

The above code fragment shows that the JSBSim manual preamble loads the following \hologo{TeX}/\hologo{LaTeX} sources
located in the \meta{JSBSim manual root}:

\noindent
\adjustbox{center}{\verb!_init.tex!\quad \verb!_preamble.tex!\quad \verb!_local_macros.tex!\quad \verb!_glossary.tex!}

\noindent
The first, \verb!_init.tex!, being loaded before \verb!\documentclass!, contains simple low level plain \hologo{TeX}
commands. The other three files contain \hologo{LaTeX} markup code used to configure the typographic elements
of the document, the typesetting automation tools and all the aspects that make the JSBSim manual look as it looks.
The names of these files all start with an underscore (\verb!_!) intentionally: all files in
the \meta{JSBSim manual root} named with a starting underscore are not to be edited by non-\hologo{TeX}pert
contributors to the manual.
Experienced \hologo{LaTeX} users wishing to improve the document style
are welcome and encouraged to send their feedback to the maintainers of the repository.

\smallskip

Let us now see the second part of the main source. This part goes from the line containing the command
\verb!\begin{document}! to the line containing \verb!\end{document}!.
In \hologo{LaTeX} language these two commands mark the beginning and the end of the particular
\hologo{LaTeX} ``environment'' named \verb!document!.
An environment is a logical unit within the source that contains material to be typeset according to a certain style.
The special environment \verb!document! gathers all the material that we are willing to put on paper.
The typesetting process is driven by commands appearing inside the \verb!document! environment;
all that is done previously in the preamble serves to configure what is going to happen right after
the \verb!\begin{document}! command is given.

\smallskip

Now let us see how the document environment content builds up.
We call ``front matter'' the initial part of a document that has been styled as a book.
This is the case of the JSBSim manual and here is what its front matter looks like:

\smallskip

\begin{lstlisting}[%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%
    escapeend={}]
»\adjustbox{right}{\rmfamily\color{gray}(continue from previous listing)}»
%---------------------------------------BEGIN DOCUMENT
\begin{»\bfseries\color{colEnv}document»}
\frontmatter
%                                           TITLE PAGE
\input{CoverPage}
%                                       COPYRIGHT PAGE
\input{CopyrightPage}
%
\cleardoublepage
%                                    TABLE OF CONTENTS
\tableofcontents
%                                INTRODUCTORY MATERIAL
\input{Acknowledgements}
\input{Preface}
\markboth{}{}
\cleardoublepage
%                                  ONE MORE TITLE PAGE
\thispagestyle{empty}
\vspace*{\stretch{1}}
\begin{»\bfseries\color{colEnv}flushright»}
\Huge\itshape JSBSim\\
\large An open source, platform-independent,\\
flight dynamics model in C++
\end{»\bfseries\color{colEnv}flushright»}
\vspace*{\stretch{3}}
»\adjustbox{right}{\rmfamily\color{gray}(to be continued)}»
\end{lstlisting}

\smallskip

This part is marked by the command \verb|\frontmatter|.
In the front matter we include \hologo{LaTeX} code from other external files:

\noindent
\adjustbox{center}{\verb!CoverPage.tex!\quad \verb!CopyrightPage.tex!\quad \verb!Acknowledgements.tex!\quad \verb!Preface.tex!}

\noindent
These are separate sources that serve to typeset the cover page,
the declaration of copyright, the acknowledgements and the preface of the JSBSim manual.
Their content is in draft state and is going to be finalized probably when the manual will have reached
a decent degree of completeness.

Various markup commands are also present in the main matter, for instance the very important
macro \verb|\tableofcontents| that typesets automatically the table of contents.
This is a typical command that relies on information stored in auxiliary files (\verb!.aux!)
by previous \hologo{LaTeX} passes on the same document.

According to the commonly accepted book design rules, the front matter of the manual ends with
a inner titling on a right-hand, odd-numbered page (``odd page'') followed by a blank left-hand,
even-numbered page (``even page'').

\smallskip

When the front matter stuff is all set, it comes the time to typeset the various chapters of the manual.
We call ``main matter'' the central part of the document and here is what it looks like:

\smallskip

\begin{lstlisting}[%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%
    escapeend={}]
»\adjustbox{right}{\rmfamily\color{gray}(continue from previous listing)}»
%                                        MAIN CONTENTS
\mainmatter
\pagestyle{myBookPageStyle}
% Here we include the core material of the »\color{colComments}book»
\input{JSBSimReferenceManual_Contents}
»\adjustbox{right}{\rmfamily\color{gray}(to be continued)}»
\end{lstlisting}

\smallskip

\noindent
The line to keep in mind here is the one that inputs \hologo{LaTeX} code from the file:

\noindent
\adjustbox{center}{\verb!JSBSimReferenceManual_Contents.tex!}

\noindent
This file gathers all the main content material of our manual. It calls other input files, each one devoted
to a single chapter of the manual. This fragmentation of our \hologo{LaTeX} source code is intentionally designed
so that contributors can concentrate exclusively on the content.
For more details on this file see Section~\ref{sec:Files:Involved}.

\medskip

Finally, we call ``back matter'' the last part of the document.
Here is what the back matter of the JSBSim manual it looks like:

\smallskip

\begin{lstlisting}[%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%
    escapeend={}]
»\adjustbox{right}{\rmfamily\color{gray}(continue from previous listing)}»
%                                           APPENDICES
% some configuration commands from package appendix
\cleardoublepage \phantomsection
\renewcommand{\chaptername}{Appendix}
\appendixpage
\noappendicestocpagenum
\addappheadtotoc
\pagestyle{myAppendixPageStyle}

% Here we include the appendix material of the book
\begin{»\bfseries\color{colEnv}appendices»}% needs package appendix
   \input{JSBSimReferenceManual_Appendices}
\end{»\bfseries\color{colEnv}appendices»}
%                                          BIBLIOGRPHY
\cleardoublepage \phantomsection
\addcontentsline{toc}{chapter}{Bibliography}
\pagestyle{myBibliographyPageStyle}
\nocite{*}
% »\color{colComments}\verb!needs biblatex, see ./_preamble.tex!»
\printbibliography[heading=myBibliography]
%                          PRINT GLOSSARY AND ACRONYMS
\cleardoublepage
\glsaddall
\printglossaries
%                                                INDEX
\markboth{}{}
\cleardoublepage
\pagestyle{myIndexPageStyle}
\indexnote{%
In the index we have many voices, of various types. The page number
on the right reveals where those terms appear in the »book».

\medskip
{\color{red}
The index---and index entries---should be the very last thing to work
at when writing a »book»! So, do not worry about what you see »below». 
These items are going to change many times before the final version of
the »document» is released.}
}% end-of-indexnote
\phantomsection
\printindex
\markboth{}{}
\cleardoublepage
%                                           TO-DO LIST
\listoftodos[List of TO-DOs and comments]
\end{»\bfseries\color{colEnv}document»}
%-----------------------------------------END DOCUMENT
\end{lstlisting}
%---------------------------------------------------------------------------------

\noindent
Similarly to the main matter code, in the back matter we input the file:

\noindent
\adjustbox{center}{\verb!JSBSimReferenceManual_Appendices.tex!}

\noindent
that gathers the material that goes into the appendices of the manual. Also in this case this source
calls other input files, each one devoted to a single appendix.

\section{Files involved in the typesetting process}\label{sec:Files:Involved}

%---------------------------------------------------------------------------------
%\begin{figure}[p]
%   \makebox[\textwidth][c]{%
%      \includegraphics[width=1.00\textwidth]{images/jsbsim_manual_tree.pdf}
%   }\\[4pt]
%   \caption{The \LaTeX{} source file tree of JSBSim manual.}\label{fig:JSBSim:Manual:Tree}
%\end{figure}
\EnlargedFigure% needs two latex passes
    {p}% #1 <where>: t, b, p
    {images/jsbsim_manual_tree.pdf}% #2 <file>: the image file included by \includegraphics
    {width=\linewidth}% #3 <opt-list>: option list to pass to \includegraphics, e.g. width=\linewidth,rotate=0
    {The \LaTeX{} source file tree of JSBSim manual.
    }% #4 <caption>: the caption text
    {fig:JSBSim:Manual:Tree}% #5 <label>: the label

%---------------------------------------------------------------------------------

In previous section we have seen that the main matter of the manual is typeset by loading
the very important source file:

\noindent
\adjustbox{center}{\verb!JSBSimReferenceManual_Contents.tex!}

\noindent
This file is made up of other simple \verb!\input! commands that load the sources of each chapter.
Here is the listing of this file:

\smallskip

%---------------------------------------------------------------------------------
\begin{lstlisting}[%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%\color{blue}\relsize{0}\rmfamily,%
    escapeend={}]
%-----------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = »\bfseries\color{colComments}./JSBSimReferenceManual.tex»
% !TEX encoding = UTF-8
% !TEX program = pdflatex
%
%------------------------------------------- CHAPTER 1
\input{contents/QuickStart}
%------------------------------------------- CHAPTER 2
\input{contents/UsersManual}
%------------------------------------------- CHAPTER 3
\input{contents/ProgrammersManual}
%------------------------------------------- CHAPTER 4
\input{contents/FormulationManual}
%------------------------------------------- CHAPTER 5
\input{contents/CaseStudies}
%------------------------------------------- EOF
\end{lstlisting}
%---------------------------------------------------------------------------------

The source files involved in the above code are the following:

\noindent
\adjustbox{center}{\verb!QuickStart.tex!\qquad \verb!UsersManual.tex!}

\noindent
\adjustbox{center}{\verb!ProgrammersManual.tex!\qquad \verb!CaseStudies.tex!}

\noindent
which are located in the directory \meta{JSBSim manual root}\verb!/contents/!.
When adding content to the JSBSim manual contributors are supposed to modify 
these files (and/or those loaded by them).

For instance, the chapter named ``Quickstart'' is defined in the file \verb!QuickStart.tex!
that goes like:

\smallskip

%---------------------------------------------------------------------------------
\input{_lst_reset}
\input{_lst_latex}
\begin{lstlisting}[%
    %tagstyle=\color{red},%
    lineskip=-1pt,%
    escapechar=»,%
    escapebegin=\ttfamily\small,%\color{blue}\relsize{0}\rmfamily,%
    escapeend={}]
%-----------------------------------------------------
% Meta-commands for the TeXworks editor
%
% !TeX root = »\bfseries\color{colComments}../JSBSimReferenceManual.tex»
% !TEX encoding = UTF-8
% !TEX program = »\color{colComments}pdflatex»
%
\chapter{Quickstart}
\label{ch:Quickstart}

\section{Getting the source}

JSBSim can be downloaded as source code in a ``release'' --- a bundled
set of source code files (a snapshot) that was taken at some »point»
in time when the source code was thought to be stable. A release is
made at random times as the developers see fit. The code can be
downloaded as a release from the project web site at:

\noindent
\adjustbox{center}{
 % a way of making some short stuff centered horizontally
 \url{http://sourceforge.net/project/showfiles.php?group_id=19399}
}

The source code can also be retrieved from the revision management
storage location using a software tool called \prog{cvs} (Concurrent
Versions System, CVS).
...
\end{lstlisting}
%---------------------------------------------------------------------------------


\medskip

By inspecting the sequence of \verb!\input! commands in the main file and in other source files
you can easily reconstruct the source file tree organization of the JSBSim manual.
This tree is represented in Figure~\vref{fig:JSBSim:Manual:Tree}.

Note the structure of the repository where the sources are stored appropriately in the directory
\meta{JSBSim manual root}\verb!! and its subdirectories
\verb!contents/!, \verb!appendices/! and \verb!images/!.

\section{Adding content}

The textual material that one wants to add to the manual is supposed to be typeset into an appropriate file.
If required, the user may create a specific \verb!.tex! file and include its content by giving an
\verb!\input! command into the parent source file.

For instance, if one is working to the file \verb!UsersManual.tex! (the parent source file),
he or she may want to create a file

\smallskip\noindent
\adjustbox{center}{
 % a way of making some short stuff centered horizontally
 \verb!UsersManual_ConfigOpts.tex! (a child source file)
}

\smallskip\noindent
into the directory \meta{JSBSim manual root}\verb!/contents/!.
This means that, probably, the subject of the child source is how JSBSim batch runs
can be configured from the command line.
Therefore, the parent file should be edited so that the command 

\smallskip\noindent
\adjustbox{center}{
 % a way of making some short stuff centered horizontally
 \verb!\input{UsersManual_ConfigOpts.tex}!
}

\smallskip\noindent
is found at some point.

\section{Text}

Normal English text is added naturally to the manual by editing the \verb!.tex! file sources.
All special typographic elements are added by specific \hologo{LaTeX} markup commands.

\section{Bibliographic citations}
%---------------------------------------------------------------------------------
\EnlargedFigure% needs two latex passes
    {t}% #1 <where>: t, b, p
    {images/Jabref_screenshot.png}% #2 <file>: the image file included by \includegraphics
    {width=\linewidth}% #3 <opt-list>: option list to pass to \includegraphics, e.g. width=\linewidth,rotate=0
    {A screenshot of a \prog{Jabref} working session.
    }% #4 <caption>: the caption text
    {fig:Jabref:Session}% #5 <label>: the label
%---------------------------------------------------------------------------------

Bibliographic citations are easily inserted into documents with
the command

\smallskip\noindent
\adjustbox{center}{
 % a way of making some short stuff centered horizontally
 \verb!\cite{!\meta{bib-reference}\verb!}!
}

\smallskip\noindent
where \meta{bib-reference} is a label contained in a \verb!.bib! file. This file is
the bibliographic database in \hologo{BibTeX} format. As shown in 
Figure~\vref{fig:Jabref:Session}, the \verb!.bib! file is easily edited with the application
\prog{Jabref}.

\section{Code fragments}

See Chapter~\ref{ch:Best:Practices}.

\section{Math}

See Chapter~\ref{ch:Best:Practices}.

%% EOF